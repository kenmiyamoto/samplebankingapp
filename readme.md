# Sample Banking Application

## Install Dependencies
- Pull down the source code.
- Navigate to the source code directory and run `npm install`

## Running the server locally
- After completing the "Install Dependencies" step above you then do the following
  - execute "npm run dev" 
  - open a browser at localhost:3000

## Running the tests
- After completing the "Install Dependencies" step above you then do the following
  - execute "npm test"
