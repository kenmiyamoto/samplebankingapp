import React from 'react'
import {render} from 'react-dom'
import {Provider} from 'react-redux'
import Head from 'next/head'

import App from '../react/App'
import {store} from '../redux'

export default function () {
  return (
    <div>
      <Head>
        <title>Sample Banking</title>
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"/>
      </Head>
      <Provider store={store}>
        <App />
      </Provider>
    </div>
  )
}
