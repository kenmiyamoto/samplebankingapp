import {createAction, handleActions} from 'redux-actions'
import {DEPOSIT, WITHDRAW} from './actionTypes'
import accounting from 'accounting'

export const initialState = {balance: 0}

function depositReducer(state, action) {
  if (action.payload <= 0) {
    return state
  }
  return {
    ...state,
    balance: +(accounting.toFixed(state.balance + action.payload, 2)),
  }
}

function withdrawalTooLarge(state, action) {
  return state.balance - action.payload < 0
}

function withdrawalReducer(state, action) {
  if (action.payload <= 0 || withdrawalTooLarge(state, action)) {
    return state
  }
  return {
    ...state,
    balance: +(accounting.toFixed(state.balance - action.payload, 2)),
  }
}

const ledgerSliceReducer = handleActions({
  DEPOSIT: depositReducer,
  WITHDRAW: withdrawalReducer,
}, initialState)

export const makeWithdrawal = createAction(WITHDRAW)
export const makeDeposit = createAction(DEPOSIT)

// function that takes in slice such as ledger
// returns a function that expects state
export function createSliceSelector(slice) {
  return function (key) {
    return [slice][key]
  }
}

export function selectBalance(state) {
  return state.ledger.balance
}

export default ledgerSliceReducer
