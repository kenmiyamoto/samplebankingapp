import {createStore, applyMiddleware, combineReducers} from 'redux'
import {createLogger} from 'redux-logger'

import {RESET_STATE} from './actionTypes'
import ledger from './ledgerSlice'

const middleware = []
if (process.env.NODE_ENV !== ('production' && 'test')) {
  middleware.push(createLogger())
}

const appReducer = combineReducers({
  ledger,
})

function rootReducer(state, action) {
  if (action.type === RESET_STATE) {
    state = undefined
  }
  return appReducer(state, action)
}

export const store = createStore(rootReducer, applyMiddleware(...middleware))
