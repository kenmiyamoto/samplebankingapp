import {store} from '../index'
import {makeDeposit, makeWithdrawal, selectBalance, initialState} from '../ledgerSlice'
import {resetState, getCurrentBalance} from '../util'

beforeEach(() => {
  store.dispatch(resetState())
})

describe('test deposit functionality', () => {
  it('makeDeposit does the right thing', () => {
    const amount = 100
    store.dispatch(makeDeposit(amount))

    expect(getCurrentBalance({store, selector: selectBalance})).toBe(100)
  })

  it('makeDeposit of negative amount does the right thing', () => {
    const amount = -100
    store.dispatch(makeDeposit(amount))

    expect(getCurrentBalance({store, selector: selectBalance})).toBe(0)
  })

  it('makeDeposit of decimals does the right thing', () => {
    const amount = 1.22
    store.dispatch(makeDeposit(amount))

    expect(getCurrentBalance({store, selector: selectBalance})).toBe(1.22)
  })
})

describe('test withdraw functionality', () => {
  it('makeWithdrawal does the right thing', () => {
    const depositAmount = 100
    store.dispatch(makeDeposit(depositAmount))

    const withdrawalAmount = 50
    store.dispatch(makeWithdrawal(withdrawalAmount))

    expect(getCurrentBalance({store, selector: selectBalance})).toBe(50)
  })

  it('makeWithdrawal of negative amount does the right thing', () => {
    const depositAmount = 100
    store.dispatch(makeDeposit(depositAmount))

    const withdrawalAmount = -50
    store.dispatch(makeWithdrawal(withdrawalAmount))

    expect(getCurrentBalance({store, selector: selectBalance})).toBe(100)
  })

  it('makeWithdrawal larger than current balance does the right thing', () => {
    const withdrawalAmount = 100
    store.dispatch(makeWithdrawal(withdrawalAmount))

    expect(getCurrentBalance({store, selector: selectBalance})).toBe(0)
  })

  it('makeWithdrawal of decimals does the right thing', () => {
    const depositAmount = 0.43
    store.dispatch(makeDeposit(depositAmount))

    const withdrawalAmount = 0.03
    store.dispatch(makeWithdrawal(withdrawalAmount))

    expect(getCurrentBalance({store, selector: selectBalance})).toBe(0.40)
  })

})
