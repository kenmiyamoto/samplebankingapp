import {RESET_STATE} from './actionTypes'

export function resetState() {
  return {
    type: RESET_STATE,
  }
}

export const getCurrentBalance = ({store, selector: selectBalance}) => {
  const state = store.getState()
  const balance = selectBalance(state)
  return balance
}
