import React from 'react'
import PropTypes from 'proptypes'
import NumericInput from 'react-numeric-input'
import accounting from 'accounting'

import {capitalizeFirstLetter} from './util'
import * as styles from './styles.js'

function TransactionComponent({name, stateSlice, setParentState, dispatchMethod, validations=[]}) {
  
  const stateSliceName = `${name}InputValue`
  const buttonValidations = [inputNotEmpty].concat(validations)

  function handleInputChange(inputValue, stateSliceName) {
    if (inputValue === null) {
      setParentState(stateSliceName, '')
      return
    }
    const value = +(accounting.toFixed(inputValue, 2))
    setParentState(stateSliceName, value)
  }

  function handleKeyPress({event, dispatchMethod, stateSlice, stateSliceName}) {
    if (!validateButton(buttonValidations)) {
      return
    }
    if (event.key === 'Enter') {
      handleTransaction({dispatchMethod, stateSlice, stateSliceName})
    }
  }

  function handleTransaction({dispatchMethod, stateSliceName, stateSlice}) {
    dispatchMethod(stateSlice)
    setParentState(stateSliceName, '')
  }

  function validateButton(validations) {
    let validationsPassed = true
    for (let validation of validations) {
      if (!validation()) {
        validationsPassed = false
        break;
      }
    }
    return validationsPassed
  }

  // default validation disables button if input is empty 
  function inputNotEmpty() {
    return stateSlice !== ''
  }

  return (
    <div className='container'>
      <div className='title'>
        {capitalizeFirstLetter(name)} Money
      </div>
      <div className='input-label'>
        Amount
      </div>
      <div className='input-wrapper'>
        <NumericInput 
          className={`${name}-input`}
          min={0.00}
          style={numericInputStyles}
          precision={2}
          value={stateSlice}
          onKeyPress={event => handleKeyPress({event, dispatchMethod, stateSliceName, stateSlice})}
          onChange={e => handleInputChange(e, stateSliceName)} />
        <div className='button-container'>
          <button
            disabled={!validateButton(buttonValidations)}
            onClick={() => handleTransaction({dispatchMethod, stateSliceName, stateSlice})}
            className={validateButton(buttonValidations) ? `${name}-button` : 'disabled-button'}>
            {capitalizeFirstLetter(name)}
          </button>
        </div>
      </div>
      <style jsx>{`
          .container {
            margin-bottom: 20px;
            height: 100px;
            background: white;
            border-radius: 4px;
            padding: 25px;
            border: 1px solid ${styles.lightGrey};
          }
          .title {
            margin-bottom: 20px;
          }
          .input-label {
            font-size: 15px;
            color: ${styles.darkGrey};
          }
          .input-wrapper {
            display: flex;
          }
          .button-container {
            position: relative;
            margin-left: 15px;
          }
          .deposit-button, .withdraw-button {
            position: absolute;
            width: 120px;
            height: 35px;
            background: ${styles.green};
            color: white;
            border: 0;
            border-radius: 4px;
            font-size: 16px;
            cursor: pointer;
            box-shadow: 0 6px ${styles.darkGreen};
          }
          .disabled-button {
            position: absolute;
            width: 120px;
            height: 35px;
            background: ${styles.grey};
            color: white;
            border: 0;
            border-radius: 4px;
            font-size: 16px;
            top: 4px;
          }
          .deposit-button:hover, .withdraw-button:hover {
            top: 2px;
            box-shadow: 0 4px ${styles.darkGreen};
          }
        `}</style>
    </div>
  )
}

const numericInputStyles = {
  'input:not(.form-control)': { 
    border: `1px solid ${styles.lightGrey}`,
    height: '40px', 
    fontSize: '20px',
    width: '150px',
  },
}

TransactionComponent.propTypes = {
  name: PropTypes.string.isRequired,
  stateSlice: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  setParentState: PropTypes.func.isRequired,
  dispatchMethod: PropTypes.func.isRequired,
  validations: PropTypes.array,
}

export default TransactionComponent
