import React from 'react'
import PropTypes from 'proptypes'
import {connect} from 'react-redux'
import accounting from 'accounting'

import {makeWithdrawal, makeDeposit, selectBalance} from '../redux/ledgerSlice'
import TransactionComponent from './Transaction'
import * as styles from './styles.js'

export class _App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      depositInputValue: '',
      withdrawInputValue: '',
    }
    this.setParentState = this.setParentState.bind(this)
    this.withdrawLargerThanBalance = this.withdrawLargerThanBalance.bind(this)
  }

  setParentState(stateSliceName, value) {
    this.setState({[stateSliceName]: value})
  }

  // validation for withdraw input disables button 
  // when withdraw request is large than balance
  withdrawLargerThanBalance() {
    return this.props.balance - this.state.withdrawInputValue >= 0
  }

  render () {
    return (
      <div>
        <div className='header'>
          <div className='title'>
            Sample Banking App
          </div>
        </div>
        <div className='content-body'>
          <div className='content-header'>
            <div className='balance-label'>Current Balance</div>
            <div className='balance-amount'>{accounting.formatMoney(this.props.balance)}</div>
          </div>
          <div className='transaction'>
            <TransactionComponent
              name='deposit'
              dispatchMethod={this.props.handleDeposit}
              stateSlice={this.state.depositInputValue}
              setParentState={this.setParentState}
            />
          </div>
          <div className='transaction'>
            <TransactionComponent
              name='withdraw'
              dispatchMethod={this.props.handleWithdrawal}
              stateSlice={this.state.withdrawInputValue}
              setParentState={this.setParentState}
              validations={[this.withdrawLargerThanBalance]}
            />
          </div>
        </div>
        <style jsx global>{`
          body {
            padding: 0;
            margin: 0;
            background: ${styles.darkWhite};
            font-size: 20px;
            font-family: Roboto, sans-serif;
          }
        `}</style>
        <style jsx>{`
          .header {
            background: white;
            padding: 20px;
            box-shadow: 0 2px 2px 0 rgba(0,0,0,0.16);
          }
          .title {
            font-weight: bold;
          }
          .content-body {
            padding: 20px 30px;
          }
          .content-header {
            margin-bottom: 20px;
          }
          .balance-label {
            font-size: 15px;
            color: ${styles.darkGrey};
          }
          .transaction {
            width: 100%;
          }
          @media only screen and (min-width: 768px) {
            .transaction {
              width: 50%;
            }
          }
          @media only screen and (min-width: 1224px) {
            .transaction {
              width: 30%;
            }
          }
        `}</style>
      </div>
    )
  }
}

_App.propTypes = {
  balance: PropTypes.number.isRequired,
  handleDeposit: PropTypes.func.isRequired,
  handleWithdrawal: PropTypes.func.isRequired,
}

function mapStateToProps (state) {
  return {
    balance: selectBalance(state),
  }
}

function mapDispatchToProps (dispatch) {
  return {
    handleDeposit(amount) {
      dispatch(makeDeposit(amount))
    },
    handleWithdrawal(amount) {
      dispatch(makeWithdrawal(amount))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(_App)
