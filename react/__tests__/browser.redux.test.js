import React from 'react'
import {Provider} from 'react-redux'
import {mount} from 'enzyme'

import {store} from '../../redux'
import {selectBalance} from '../../redux/ledgerSlice'
import {resetState, getCurrentBalance} from '../../redux/util'
import {createInputUpdater, createElementClicker} from '../browserTestHelpers'
import App from '../App'

const updateDepositInput = createInputUpdater({name: '.deposit-input'})

const updateWithdrawInput = createInputUpdater({name: '.withdraw-input'})

const clickDepositButton = createElementClicker({name: '.deposit-button'})

const clickWithdrawButton = createElementClicker({name: '.withdraw-button'})

const clickTitle = createElementClicker({name: '.title'})

const wrapper = mount(
  <Provider store={store}>
    <App />
  </Provider>
)

beforeEach(() => {
  store.dispatch(resetState())
})

describe('testing connected component', () => {
  
  describe('testing deposit functionality', () => {
  
    it('clicking deposit button updates state correctly', () => {

      updateDepositInput({wrapper, amount: 100})

      const initialBalance = getCurrentBalance({store, selector: selectBalance})

      expect(initialBalance).toBe(0)

      clickDepositButton({wrapper})

      const newBalance = getCurrentBalance({store, selector: selectBalance})

      expect(newBalance).toBe(100)

    })

    it('depositing amount that includes cents updates state correctly', () => {

      const amount = 2.24

      updateDepositInput({wrapper, amount})

      const initialBalance = getCurrentBalance({store, selector: selectBalance})

      expect(initialBalance).toBe(0)

      clickDepositButton({wrapper})

      const newBalance = getCurrentBalance({store, selector: selectBalance})

      expect(newBalance).toBe(amount)

    })
  })

  describe('testing withdraw functionality', () => {
    
    it('clicking withdraw button updates state correctly', () => {

      updateDepositInput({wrapper, amount: 125})

      clickDepositButton({wrapper})

      const balanceAfterDeposit = getCurrentBalance({store, selector: selectBalance})

      expect(balanceAfterDeposit).toBe(125)

      updateWithdrawInput({wrapper, amount: 50})

      clickWithdrawButton({wrapper})

      const balanceAfterWithdrawal = getCurrentBalance({store, selector: selectBalance})

      expect(balanceAfterWithdrawal).toBe(75)

    })

    it('withdrawing exact balance works correctly', () => {

      const amount = 42

      updateDepositInput({wrapper, amount})

      clickDepositButton({wrapper})

      updateWithdrawInput({wrapper, amount})

      clickWithdrawButton({wrapper})

      const balanceAfterWithdrawal = getCurrentBalance({store, selector: selectBalance})

      expect(balanceAfterWithdrawal).toBe(0)

    })

    it('withdrawing amount that includes cents works correctly', () => {

      updateDepositInput({wrapper, amount: 2.43})

      clickDepositButton({wrapper})

      updateWithdrawInput({wrapper, amount: 1.03})

      clickWithdrawButton({wrapper})

      const balanceAfterWithdrawal = getCurrentBalance({store, selector: selectBalance})

      expect(balanceAfterWithdrawal).toBe(1.40)
    })
  })
})
