export function createInputUpdater({name}) {
  return function({wrapper, amount}) {
    wrapper.find(name).simulate('change', {target: {value: amount}})
  }
}

export function createElementClicker({name}) {
  return function({wrapper}) {
    wrapper.find(name).simulate('click')
  }
}
